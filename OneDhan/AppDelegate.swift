//
//  AppDelegate.swift
//  OneDhan
//
//  Created by Ankit on 31/07/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit
import MBProgressHUD
import LGSideMenuController
import IQKeyboardManager
@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var sidemenucontroller : LGSideMenuController?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared().isEnabled = true
        if let userLoggedIn = UserDefaults.standard.value(forKey: "isUserLoggedIn") as? Bool
        {
            if userLoggedIn == true {
                setupSideMenu()
            }
        }
        
        // Override point for customization after application launch.
        return true
    }
    
    func setupSideMenu()
    {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home: HomeViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        let sideMenuVC = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
        let controller = LGSideMenuController.init(rootViewController: homeNavigation, leftViewController: sideMenuVC, rightViewController: nil)
        self.window?.rootViewController = controller
        self.window?.makeKeyAndVisible()
    }
    
    
    
}

