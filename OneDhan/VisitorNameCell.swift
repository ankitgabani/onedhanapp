//
//  VisitorNameCell.swift
//  OneDhan
//
//  Created by Ankit on 01/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit
import MBProgressHUD

class VisitorNameCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var imgCha: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblPhone: UILabel!
    
    @IBOutlet weak var lblDatye: UILabel!
    
    @IBOutlet weak var imgVisit: UIImageView!
    
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var btnPhoneCall: UIButton!
    
    @IBOutlet weak var imgLocationCont: NSLayoutConstraint!
    
    @IBOutlet weak var imgChaCont: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        imgCha.image = imgCha.image?.withRenderingMode(.alwaysTemplate)
        imgCha.tintColor = UIColor(red: 0/255, green: 187/255, blue: 174/255, alpha: 1)

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
