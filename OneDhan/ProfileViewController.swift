//
//  ProfileViewController.swift
//  OneDhan
//
//  Created by Ankit on 01/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit
import MBProgressHUD

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var alertViw: UIView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var lblErrorFirstName: UILabel!
    
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var lblLastName: UILabel!
    
    @IBOutlet weak var txtCompany: UITextField!
    @IBOutlet weak var lblCompanmy: UILabel!
    
    @IBOutlet weak var txtMobileNumber: UITextField!
    
    
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var lblState: UILabel!
    
    @IBOutlet weak var txtDis: UITextField!
    @IBOutlet weak var lblDis: UILabel!
    
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var lblCity: UILabel!
    
    var isFromHome = false
    
    var isLogout = false
    override func viewDidLoad() {
        super.viewDidLoad()
        alertViw.isHidden = true
        self.setShadowinHeader(headershadowView: [mainView])
        callFetchData()
        NotificationCenter.default.addObserver(self, selector: #selector(onNotificationLogout(notification:)), name: Notification.Name.myNotificationKeyLogout, object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    @objc func onNotificationLogout(notification:Notification)
    {
        alertViw.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        alertViw.isHidden = true
        
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnCancleLog(_ sender: Any) {
        alertViw.isHidden = true
    }
    
    @IBAction func btnConfirmLog(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
        UserDefaults.standard.synchronize()
        
        let controller = AppUtilites.sharedInstance.getViewController(controllerName: "LoginViewController")
        let navigation = UINavigationController(rootViewController: controller)
        
        navigation.navigationBar.isHidden = true
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController = navigation
            appDelegate.window?.makeKeyAndVisible()
        }
    }
    
    func setShadowinHeader(headershadowView: [UIView]? = nil, shadowBtn: [UIButton]? = nil, setRound: Bool = false) {
        // Shadow and Radius
        if headershadowView != nil {
            for i in headershadowView! {
                i.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
                i.layer.shadowOffset = CGSize(width: 0, height: 3)
                i.layer.shadowOpacity = 2
                i.layer.shadowRadius = 3
                i.layer.masksToBounds = false
                if setRound == true {
                    i.layer.cornerRadius = i.frame.height/2
                }
            }
        }
        
        if shadowBtn != nil {
            for i in shadowBtn! {
                i.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
                i.layer.shadowOffset = CGSize(width: 0, height: 3)
                i.layer.shadowOpacity = 2
                i.layer.shadowRadius = 3
                i.layer.masksToBounds = false
                if setRound == true {
                    i.layer.cornerRadius = i.frame.height/2
                }
            }
        }
    }
    
    @IBAction func btnSaveChange(_ sender: Any) {
        
        if self.isValidatedReset() {
            callUpdateProfile()
        }
        
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        if isFromHome == true{
            self.navigationController?.popViewController(animated: true)
        } else {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.setupSideMenu()
        }
        
    }
    
    // MARK:- Validation
    func isValidatedReset() -> Bool {
        self.lblErrorFirstName.isHidden = true
        self.lblLastName.isHidden = true
        self.lblCompanmy.isHidden = true
        self.lblState.isHidden = true
        self.lblDis.isHidden = true
        self.lblCity.isHidden = true
        
        if txtFirstName.text == "" {
            self.lblErrorFirstName.isHidden = false
            return false
        } else if txtLastName.text == "" {
            self.lblLastName.isHidden = false
            return false
        } else if txtCompany.text == "" {
            self.lblCompanmy.isHidden = false
            return false
        } else if txtState.text == "" {
            self.lblState.isHidden = false
            return false
        } else if txtDis.text == "" {
            self.lblDis.isHidden = false
            return false
        } else if txtCity.text == "" {
            self.lblCity.isHidden = false
            return false
        }
        
        self.lblErrorFirstName.isHidden = true
        self.lblLastName.isHidden = true
        self.lblCompanmy.isHidden = true
        self.lblState.isHidden = true
        self.lblDis.isHidden = true
        self.lblCity.isHidden = true
        
        return true
    }
    
    //MARK:- API Call
    func callFetchData() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let phonenumber = UserDefaults.standard.value(forKey: "UserPhone") as? String
        
        let param = ["mobile": phonenumber ?? ""]
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost("mapi/profile-fetch.php", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    let objStatusCode = response?.value(forKey: "statusCode") as? Int
                    let message = response?.value(forKey: "message") as? String
                    
                    if objStatusCode == 200 {
                        let city = response?.value(forKey: "city") as? String
                        let company = response?.value(forKey: "company") as? String
                        let district = response?.value(forKey: "district") as? String
                        let firstname = response?.value(forKey: "firstname") as? String
                        let lastname = response?.value(forKey: "lastname") as? String
                        let mobile = response?.value(forKey: "mobile") as? String
                        let state = response?.value(forKey: "state") as? String
                        
                        self.txtFirstName.text = firstname ?? ""
                        self.txtCity.text = city ?? ""
                        self.txtDis.text = district ?? ""
                        self.txtLastName.text = lastname ?? ""
                        self.txtMobileNumber.text = mobile ?? ""
                        self.txtState.text = state ?? ""
                        self.txtCompany.text = company ?? ""
                    } else {
                        self.view.makeToast(message)
                    }
                    
                    
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
        
    }
    
    func callUpdateProfile() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let phonenumber = UserDefaults.standard.value(forKey: "UserPhone") as? String
        
        let param = ["mobile": phonenumber ?? "","firstname": txtFirstName.text!,"lastname": txtLastName.text!,"company": txtCompany.text!,"state": txtState.text!,"city": txtCity.text!,"district": txtDis.text!]
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost("mapi/modify-signup.php", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    let objStatusCode = response?.value(forKey: "statusCode") as? Int
                    let message = response?.value(forKey: "message") as? String
                    
                    if objStatusCode == 200 {
                        self.view.makeToast(message)
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
        
    }
}


//url = http://onedhan.co/app/dist/api/mapi/profile-fetch.php
//["mobile": "7096859504"]
//STATUS CODE Optional(200)
//Response Optional({
//    city = city;
//    company = surat;
//    district = Tssg;
//    firstname = Ankit;
//    lastname = Patel;
//    message = "user found!";
//    mobile = 7096859504;
//    state = state;
//    statusCode = 200;
//})
