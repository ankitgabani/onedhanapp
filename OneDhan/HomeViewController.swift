//
//  HomeViewController.swift
//  OneDhan
//
//  Created by Ankit on 01/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit
import MBProgressHUD
import Foundation
import MapKit

class HomeViewController: UIViewController,MKMapViewDelegate,CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var imgQRCode: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var alertView: UIView!
    
    var locationManager: CLLocationManager!
    
    var objLatitude: Double?
    var objLongitude: Double?

    var objFullAddrress = String()

    
    var isLogout = false
    override func viewDidLoad() {
        super.viewDidLoad()
        callAuthenticateQRCode()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.distanceFilter = 10
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        alertView.isHidden = true
        callFetchvdashboard()
        self.setShadowinHeader(headershadowView: [topView])
        self.setShadowinHeader(headershadowView: [bottomView])
        
        let phonenumber = UserDefaults.standard.value(forKey: "UserPhone") as? String
        
        let imageQR = generateQRCode(from: phonenumber!)
        imgQRCode.image = imageQR

        NotificationCenter.default.addObserver(self, selector: #selector(onNotificationLogout(notification:)), name: Notification.Name.myNotificationKeyLogout, object: nil)

        // Do any additional setup after loading the view.
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.last {
            
            objLatitude = location.coordinate.latitude
            objLongitude = location.coordinate.longitude
            
            let geoCoder = CLGeocoder()
            let location = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            geoCoder.reverseGeocodeLocation(location, completionHandler:
                {
                    placemarks, error -> Void in
                    
                    // Place details
                    guard let placeMark = placemarks?.first else { return }
                    
                    var objLocation = String()
                    var objCity = String()
                    var objCountry = String()
                    // Location name
                    if let locationName = placeMark.location {
                        print(locationName)
                    }
                    // Street address
                    if let street = placeMark.thoroughfare {
                        print(street)
                        objLocation = street
                    }
                    // City
                    if let city = placeMark.subAdministrativeArea {
                        print(city)
                        objCity = city
                    }
                    // Zip code
                    if let zip = placeMark.isoCountryCode {
                        print(zip)
                    }
                    // Country
                    if let country = placeMark.country {
                        print(country)
                        objCountry = country
                    }
                    
                    self.objFullAddrress = "\(objLocation), \(objCity), \(objCountry)"
            })
            
        }
    }
    
    @objc func onNotificationLogout(notification:Notification)
    {
        alertView.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        alertView.isHidden = true
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnReport(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ReportViewController") as! ReportViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnCancelLogout(_ sender: Any) {
        alertView.isHidden = true
    }
    
    @IBAction func btnConfilrLogout(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
        UserDefaults.standard.synchronize()
        
        let controller = AppUtilites.sharedInstance.getViewController(controllerName: "LoginViewController")
        let navigation = UINavigationController(rootViewController: controller)
        
        navigation.navigationBar.isHidden = true
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController = navigation
            appDelegate.window?.makeKeyAndVisible()
        }
    }
    
    @IBAction func btnScan(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "QRScannerController") as! QRScannerController
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func btnNew(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "NewVisitorViewController") as! NewVisitorViewController
        vc.isFromHome = true
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func btnProfile(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ProfileViewController") as! ProfileViewController
        vc.isFromHome = true
        self.navigationController?.pushViewController(vc, animated: true)

    }
  
    func setShadowinHeader(headershadowView: [UIView]? = nil, shadowBtn: [UIButton]? = nil, setRound: Bool = false) {
        // Shadow and Radius
        if headershadowView != nil {
            for i in headershadowView! {
                i.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
                i.layer.shadowOffset = CGSize(width: 0, height: 3)
                i.layer.shadowOpacity = 2
                i.layer.shadowRadius = 3
                i.layer.masksToBounds = false
                if setRound == true {
                    i.layer.cornerRadius = i.frame.height/2
                }
            }
        }
        
        if shadowBtn != nil {
            for i in shadowBtn! {
                i.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
                i.layer.shadowOffset = CGSize(width: 0, height: 3)
                i.layer.shadowOpacity = 2
                i.layer.shadowRadius = 3
                i.layer.masksToBounds = false
                if setRound == true {
                    i.layer.cornerRadius = i.frame.height/2
                }
            }
        }
    }
    
    
    //MARK:- API Call
    func callFetchvdashboard() {
        //MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let phonenumber = UserDefaults.standard.value(forKey: "UserPhone") as? String

        let param = ["mobile": phonenumber ?? ""]
         print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPostArray("mapi/fetchvdashboard.php", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                 
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
    }
    
    func callAuthenticateQRCode() {
           MBProgressHUD.showAdded(to: self.view, animated: true)
           
           let phonenumber = UserDefaults.standard.value(forKey: "UserPhone") as? String

        let param = ["mobile": phonenumber ?? "","credential": "9104435733", "latitude": "\(objLatitude ?? 0.0)","longitude": "\(objLongitude ?? 0.0)","location": objFullAddrress]
            print(param)
           APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPostArray("mapi/authenticate.php", parameters: param, completionHandler: { (response, error, statusCode) in
               
               if error == nil {
                   print("STATUS CODE \(String(describing: statusCode))")
                   print("Response \(String(describing: response))")
                   
                   if statusCode == 200 {
                       MBProgressHUD.hide(for: self.view, animated: true)
                       
                    
                   } else {
                       MBProgressHUD.hide(for: self.view, animated: true)
                   }
                   
               } else {
                   MBProgressHUD.hide(for: self.view, animated: true)
                   print("Response \(String(describing: response))")
               }
           })
       }

}
