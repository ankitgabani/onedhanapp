//
//  NewVisitorViewController.swift
//  OneDhan
//
//  Created by Ankit on 01/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit
import MBProgressHUD
import MapKit

class NewVisitorViewController: UIViewController,MKMapViewDelegate,CLLocationManagerDelegate,UITextFieldDelegate {
    
    
    @IBOutlet weak var alertViewNewAdd: UIView!
    
    @IBOutlet weak var lblMsgresponse: UILabel!
    
    
    @IBOutlet weak var alertView: UIView!
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var lnlErrorName: UILabel!
    
    @IBOutlet weak var lblErrorPhone: UILabel!
    
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var NewMapView: MKMapView!
    
    var locationManager: CLLocationManager!
    var isFromHome = false
    
    var objLatitude: Double?
    var objLongitude: Double?

    var objFullAddrress = String()
    
    var isLogout = false
    override func viewDidLoad() {
        super.viewDidLoad()
         txtPhone.delegate = self
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.distanceFilter = 10
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        NewMapView.delegate = self
        self.setShadowinHeader(headershadowView: [mainView])
        alertView.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(onNotificationLogout(notification:)), name: Notification.Name.myNotificationKeyLogout, object: nil)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnOKMSG(_ sender: Any) {
        alertViewNewAdd.isHidden = true
    }
    
    
    @objc func onNotificationLogout(notification:Notification)
    {
        alertView.isHidden = false
    }
    override func viewWillAppear(_ animated: Bool) {
           alertView.isHidden = true
           
       }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string:
        String) -> Bool {
        
        if textField == txtPhone {
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        return true
    }
    
    @IBAction func btnConfirmLog(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
        UserDefaults.standard.synchronize()
        
        let controller = AppUtilites.sharedInstance.getViewController(controllerName: "LoginViewController")
        let navigation = UINavigationController(rootViewController: controller)
        
        navigation.navigationBar.isHidden = true
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController = navigation
            appDelegate.window?.makeKeyAndVisible()
        }
    }
    
    @IBAction func btnCancelLog(_ sender: Any) {
        alertView.isHidden = true
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.last {
            
            objLatitude = location.coordinate.latitude
            objLongitude = location.coordinate.longitude
            
            let geoCoder = CLGeocoder()
            let location = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            geoCoder.reverseGeocodeLocation(location, completionHandler:
                {
                    placemarks, error -> Void in
                    
                    // Place details
                    guard let placeMark = placemarks?.first else { return }
                    
                    var objLocation = String()
                    var objCity = String()
                    var objCountry = String()
                    // Location name
                    if let locationName = placeMark.location {
                        print(locationName)
                    }
                    // Street address
                    if let street = placeMark.thoroughfare {
                        print(street)
                        objLocation = street
                    }
                    // City
                    if let city = placeMark.subAdministrativeArea {
                        print(city)
                        objCity = city
                    }
                    // Zip code
                    if let zip = placeMark.isoCountryCode {
                        print(zip)
                    }
                    // Country
                    if let country = placeMark.country {
                        print(country)
                        objCountry = country
                    }
                    
                    self.objFullAddrress = "\(objLocation), \(objCity), \(objCountry)"
            })
            
        }        
    }
    
    // MARK:- Validation
    func isValidatedReset() -> Bool {
        self.lnlErrorName.isHidden = true
        self.lblErrorPhone.isHidden = true
        
        if txtName.text == "" {
            self.lnlErrorName.isHidden = false
            return false
        } else if txtPhone.text == "" {
            self.lblErrorPhone.isHidden = false
            return false
        }
        
        self.lnlErrorName.isHidden = true
        self.lblErrorPhone.isHidden = true
        
        return true
    }
    @IBAction func btnSubmit(_ sender: Any) {
        
        if self.isValidatedReset() {
            callLogin()
        }
        
    }
    
    @IBAction func btnCancle(_ sender: Any) {
        if isFromHome == true{
            self.navigationController?.popViewController(animated: true)
        } else {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.setupSideMenu()
        }
        
    }
    
    func setShadowinHeader(headershadowView: [UIView]? = nil, shadowBtn: [UIButton]? = nil, setRound: Bool = false) {
        // Shadow and Radius
        if headershadowView != nil {
            for i in headershadowView! {
                i.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
                i.layer.shadowOffset = CGSize(width: 0, height: 3)
                i.layer.shadowOpacity = 2
                i.layer.shadowRadius = 3
                i.layer.masksToBounds = false
                if setRound == true {
                    i.layer.cornerRadius = i.frame.height/2
                }
            }
        }
        
        if shadowBtn != nil {
            for i in shadowBtn! {
                i.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
                i.layer.shadowOffset = CGSize(width: 0, height: 3)
                i.layer.shadowOpacity = 2
                i.layer.shadowRadius = 3
                i.layer.masksToBounds = false
                if setRound == true {
                    i.layer.cornerRadius = i.frame.height/2
                }
            }
        }
    }
    
    //MARK:- API Call
    func callLogin() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let phonenumber = UserDefaults.standard.value(forKey: "UserPhone") as? String
        
        let param = ["mobile": phonenumber ?? "","visitorname": txtName.text!, "visitornumber": txtPhone.text!,"latitude": "\(objLatitude ?? 0.0)","longitude": "\(objLongitude ?? 0.0)","location": objFullAddrress]
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost("mapi/insert-visitor.php", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    
                    let objStatusCode = response?.value(forKey: "statusCode") as? Int
                    let message = response?.value(forKey: "message") as? String
                    let otpsent = response?.value(forKey: "otpsent") as? Int
                    
                    if objStatusCode == 200 {
                        MBProgressHUD.hide(for: self.view, animated: true)

                        self.callSendPushNotification()
                        
                        self.alertViewNewAdd.isHidden = false
                        self.lblMsgresponse.text = message
                        
                        self.txtPhone.text = ""
                        self.txtName.text = ""
                      //  self.view.makeToast(message)
                    } else {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.txtPhone.text = ""
                        self.txtName.text = ""
                        self.alertViewNewAdd.isHidden = false
                        self.lblMsgresponse.text = message
                    }
                    
                    
                } else {
                    self.txtPhone.text = ""
                    self.txtName.text = ""
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
    }
    
    func callSendPushNotification() {
        //MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let phonenumber = UserDefaults.standard.value(forKey: "UserPhone") as? String
        
        let param = ["mobile": phonenumber ?? "","credential": txtPhone.text!]
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost("mapi/pushnotification.php", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    let objStatusCode = response?.value(forKey: "statusCode") as? Int
                    let arrResults = response?.value(forKey: "results") as? NSArray
                    let failure = response?.value(forKey: "failure") as? Int
                    
                    if failure == 0 {
                        
                        let message_id = arrResults?.value(forKey: "message_id") as? String
                        
                    } else {
                        let error = arrResults?.value(forKey: "error") as? String
                        self.view.makeToast(error)
                    }
                    
                } else {
                    self.txtPhone.text = ""
                    self.txtName.text = ""
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
        
    }
}


//url = http://onedhan.co/app/dist/api/mapi/pushnotification.php
//["mobile": "7096859504", "credential": "8741032012"]
//STATUS CODE Optional(200)
//Response Optional({
//    "canonical_ids" = 0;
//    failure = 1;
//    "multicast_id" = 918303956202878822;
//    results =     (
//                {
//            error = InvalidRegistration;
//        }
//    );
//    success = 0;
//})


//url = http://onedhan.co/app/dist/api/mapi/pushnotification.php
//["mobile": "7096859504", "credential": "9104435733"]
//STATUS CODE Optional(200)
//Response Optional({
//    "canonical_ids" = 0;
//    failure = 0;
//    "multicast_id" = 5277358438016641177;
//    results =     (
//                {
//            "message_id" = "0:1596433658652899%5c319dd25c319dd2";
//        }
//    );
//
