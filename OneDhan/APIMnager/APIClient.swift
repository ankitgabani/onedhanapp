//
//  APIClient.swift
//  Luxury Library
//
//  Created by Gabani on 08/06/20.
//  Copyright © 2020 Ankit Gabani. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD
import UIKit
import MBProgressHUD

class APIClient: NSObject {
    
    class var sharedInstance: APIClient {
        
        struct Static {
            static let instance: APIClient = APIClient()
        }
        return Static.instance
    }
    
    var responseData: NSMutableData!
    
    func MakeAPICallWithoutAuthHeaderPost(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        print(parameters)
        
        if NetConnection.isConnectedToNetwork() == true
        {
            AF.request(BASE_URL + url, method: .post, parameters: parameters, encoding: URLEncoding(destination: .methodDependent), headers: [:]).responseJSON { response in
                
                switch(response.result) {
                    
                case .success:
                    if response.value != nil{
                        if let responseDict = ((response.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                case .failure:
                    print(response.error!)
                    completionHandler(nil, response.error, response.response?.statusCode)
                }
            }
        }
        else
        {
            print("No Network Found!")
            SVProgressHUD.dismiss()
        }
    }
    
    func MakeAPICallWithoutAuthHeaderPostArray(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSArray?, Error?, Int?) -> Void) {
          
          print("url = \(BASE_URL + url)")
          print(parameters)
          
          if NetConnection.isConnectedToNetwork() == true
          {
              AF.request(BASE_URL + url, method: .post, parameters: parameters, encoding: URLEncoding(destination: .methodDependent), headers: [:]).responseJSON { response in
                  
                  switch(response.result) {
                      
                  case .success:
                      if response.value != nil{
                          if let responseDict = ((response.value as AnyObject) as? NSArray) {
                              completionHandler(responseDict, nil, response.response?.statusCode)
                          }
                      }
                  case .failure:
                      print(response.error!)
                      completionHandler(nil, response.error, response.response?.statusCode)
                  }
              }
          }
          else
          {
              print("No Network Found!")
              SVProgressHUD.dismiss()
          }
      }
    
    func MakeAPICallWithoutAuthHeaderPostVisitorReports(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        print(parameters)
        
        if NetConnection.isConnectedToNetwork() == true
        {
            AF.request("http://onedhan.co/app/dist/api/mapi/fetch-visitor.php", method: .post, parameters: parameters, encoding: URLEncoding(destination: .methodDependent), headers: [:]).responseJSON { response in
                
                switch(response.result) {
                    
                case .success:
                    if response.value != nil{
                        if let responseDict = ((response.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                case .failure:
                    print(response.error!)
                    completionHandler(nil, response.error, response.response?.statusCode)
                }
            }
        }
        else
        {
            print("No Network Found!")
            SVProgressHUD.dismiss()
        }
    }
    
    
    func showIndicator(){
        SVProgressHUD.show()
    }
    
    func hideIndicator(){
        SVProgressHUD.dismiss()
    }
    
    func showSuccessIndicator(message: String){
        SVProgressHUD.showSuccess(withStatus: message)
    }
}

